FROM ubuntu:16.04

RUN apt-get update && apt-get install -y software-properties-common
RUN add-apt-repository ppa:ubuntugis/ppa && apt-get update && apt-get install -y \
    python \
    python-pip \
    libboost-dev \
    libboost-serialization-dev \
    gdal-bin libgdal-dev make cmake libbz2-dev libexpat1-dev swig python-dev git curl wget

WORKDIR /app
RUN git clone https://github.com/cyang-kth/fmm.git
RUN cd fmm && mkdir build && cd build && cmake .. && make -j4 && make install
